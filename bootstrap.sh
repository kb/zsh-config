#!/bin/bash
if [ -e $HOME/.zshrc -o -e $HOME/.zsh -o -e $HOME/.zprofile ];then
    echo "$HOME/.zsh* exists. Bailing out";
    exit 1;
fi
cd $HOME
ln -s dotfiles/zsh .zsh
ln -s .zsh/zshrc .zshrc
ln -s .zsh/zprofile .zprofile
