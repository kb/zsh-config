#!/bin/zsh
# width
local width=60
# use unicode bar chars?
local fancy_bar=1
# interval to output data
local interval=1
# interface to watch
local iface=eth0
# current absolute value
local tx=0
local rx=0
# value before
local old_tx=0
local old_rx=0
# current speed
local diff_tx=0
local diff_rx=0
# max speed
local max_tx=0
local max_rx=0

if (( $fancy_bar ));then
    bar=(_ ▁ ▂ ▃ ▄ ▅ ▆ ▇ █)
else
    bar=(. . _ _ : : = = \|)
fi

# get the current speed
get_traffic() {
    raw_data=($(cat /proc/net/dev | grep $iface | tr -s ' ' | cut -d: -f2))

    old_tx=$tx
    tx=$raw_data[9]
    diff_tx=$(( $tx - $old_tx ))

    old_rx=$rx
    rx=$raw_data[9]
    diff_rx=$(( $rx - $old_rx ))
}

# calculate the current max value
calc_max() {
    if [[ $diff_tx > $max_tx ]]; then
        max_tx=$diff_tx
    fi
    if (( $diff_rx > $max_rx )); then
        max_rx=$diff_rx
    fi
}

get_traffic
while $(true); do
    get_traffic
    calc_max
    bar_tx=$bar[$(( 10 * $diff_tx / ( $max_tx + 1)))]
    bar_rx=$bar[$(( 10 * $diff_rx / ( $max_rx + 1)))]
    sleep $interval
done
