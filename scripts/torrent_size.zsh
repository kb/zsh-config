#!/bin/zsh
local file_sizes
local total
local factor=0
zparseopts -E -D m=mb g=gb k=kb
if [[ -n $kb ]]; then
    factor=1
elif [[ -n $mb ]]; then
    factor=2
elif [[ -n $gb ]]; then
    factor=3
fi

file_sizes=($(cat $@ |grep -ao 'lengthi[0-9]*' | grep -o '[0-9]*'))
for i ($file_sizes) { ((total += $i)) } 

echo $(($total / 1024**$factor))
