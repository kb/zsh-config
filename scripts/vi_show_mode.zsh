# source this to get into vi-mode with the mode showing as rprompt
bindkey -v

function my-accept-line {
 POSTDISPLAY=""
 zle .accept-line
}

zle -N my-accept-line
bindkey "^M" my-accept-line
bindkey "^J" my-accept-line

setopt prompt_subst;

function zle-line-init zle-keymap-select {
  RPS1="${${KEYMAP/vicmd/-- NORMAL --}/(main|viins)/-- INSERT --}"
  RPS2=$RPS1
  RPS3=$RPS1
  RPS4=$RPS1
  zle reset-prompt
}
zle -N zle-line-init
zle -N zle-keymap-select
