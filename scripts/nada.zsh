#!/bin/zsh
# purpose: resolve all urls in $1 to streams, let mplayer dumpstream them

# exit unless filename or urls
# if [[ -z $1 && -n $urls[1] ]]; then exit 1; fi

# local urls=(${urls:-$(cat $1 | grep -v '^#')})
# echo $urls
TMPDIR=/tmp/nada
for url ($urls) {
    echo "$fg_bold[green]#-- resolving$reset_color\n\t$url"
    wget --quiet -P $TMPDIR $url
    # look for mms:// links
    stream=$(cat $TMPDIR/${url:t} | grep -o '"mms.*"' | sed 's/"//g')
    # look for rtsp:// links
    if [[ -z $stream ]]; then
        stream=$(cat $TMPDIR/${url:t} | grep -o '"rtsp.*"' | sed 's/"//g')
    fi
    # continue if still no stream
    if [[ -z $stream ]]; then
        echo "$fg_bold[red] Could not extract stream from $url!"
        continue
    fi
    local_file=${stream:t}
    ln -fs $local_file CURRENT

    echo "$fg_bold[blue]streamed file$reset_color\n\t $stream"
    echo "$fg_bold[blue]local file$reset_color\n\t $PWD/$local_file"
    echo "$fg_bold[blue]To Watch progress$reset_color\n\t $bold_color watch -n.5 ls -sHh $PWD/CURRENT $reset_color"

    mplayer -really-quiet \
            -vo gl \
            -ao oss \
            -dumpstream -dumpfile $local_file $stream 
            # >/dev/null 2>/dev/null

    if (( $? > 0 )); then
        echo "$fg_bold[red] Oops, something went not right$reset_color"
        notify-send -u critical "Failed to download $url"
    fi
    rm $TMPDIR/${url:t}
    notify-send "done ripping $url"
    echo "$fg_bold[green] -- done with $url$reset_color"
}
