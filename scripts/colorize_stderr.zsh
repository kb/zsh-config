# colorize STDERR in red
exec 2>>(while read line; do
     print '\e[91m'${(q)line}'\e[0m' > /dev/tty; print -n $'\0'; done &)
