# zshrc
# sources a lot of specific scripts in $ZSH_CONFIG_DIR (variable set in zshenv)
# for interactive shells only
unset DEBUG_ZSHRC
export ZSH_CONFIG_DIR=$HOME/.zsh
config_files=(
    options
    terminal
    modules
    tools
    functions
    completion
    aliases
    prompt
    zle
    temp
)
for conf ($config_files) {
    source $ZSH_CONFIG_DIR/$conf || echo "ERROR: could not source $ZSH_CONFIG_DIR/$conf !"
    if [[ -f $ZSH_CONFIG_DIR_LOCAL/$conf ]]; then
        [[ -n $DEBUG_ZSHRC ]] && echo "loading local $conf"
        source $ZSH_CONFIG_DIR_LOCAL/$conf || echo "WARNING: (local): could not source $ZSH_CONFIG_DIR_LOCAL/$conf !"
    fi
}
        
source ~/.zsh/aliases.d/archlinux
eval $(perl -Mlocal::lib)
rehash
# vim: ft=zsh
