#{{{ Variables
#{{{ zsh
# directory to look for config files
    export ZSH_CONFIG_DIR=$HOME/.zsh
    export ZSH_CONFIG_DIR_LOCAL=$HOME/.zsh/local/$HOST
#}}}
#{{{ Java
    export JAVA_HOME=/usr/lib/jvm/java-6-openjdk
    export JAVA_HOME=/opt/java6/jre
    export AWT_TOOLKIT="XToolkit"
#}}}
#{{{ editor / pager
    export EDITOR="/usr/bin/vim"
    export HG="/usr/bin/vim"
    # export EDITOR="/home/kb/local/bin/vim"
    export VIMSESSION="/home/kb/.vim/sessions/"
    # export PAGER="most"
    export PAGER="less -R"
#}}}
#{{{ xterm / urxvt
    # NOTE This is not read by xterm but an external script
    export XTERM_FONT="Inconsolata";
#}}}
#{{{ nethack
    # found this. liked it. geeky :) especially the 'gender:male,noautopickmeup' part...
    # export NETHACKOPTIONS='gender:male,noautopickup,color,lit_corridor,showrace,showexp,showdmg,showweight,time,toptenwin,catname:Prowl,msg_window:f,!legacy'
#}}}
#{{{ local prefixes
    # export CMAKE_INSTALL_PREFIX="/home/kb/local"
    # export AWESOME_LIB="/home/kb/local/share/awesome/lib"
#}}}
#}}}
#{{{ Paths
#{{{ PATH
# make path a set (i.e. only unique items)
    typeset -U path
    path=( ~/local/bin ~/local/games /usr/local/bin $path )
#}}}
#{{{ manpath: 
    # see zshenv
#}}}
#{{{ fpath
    typeset -U fpath
    fpath=( ~/.zsh/functions.d $fpath )
#}}}
#{{{perl
    export PERL5LIB=~/local/lib/perl5
#}}}
#{{{ Python
    # typeset -U pythonpath
    # pythonpath=( /code/python )
    # export PYTHONPATH=/code/python:/usr/lib/python2.7:/usr/lib/python2.7/site-packages:$PYTHONPATH
    # export JYTHONPATH=/opt/jython/Lib/:$JYTHONPATH:$PYTHONPATH
#
#}}}
#{{{ JAVA
    CLASSPATH=/usr/share/java/mysql-jdbc/mysql-connector-java-bin.jar:$CLASSPATH
    CLASSPATH=/build/Jena-2.6.4/lib/jena-2.6.4.jar:$CLASSPATH
    export CLASSPATH
#}}}
#}}}
#{{{ ... delegate
#{{{ finally, source zshenv
    source $ZSH_CONFIG_DIR/zshenv
#}}}
#{{{ load local settings
    source $HOME/.zprofile.local
#}}}
#}}}
install -dm700 /dev/shm/firefox-cache
# vim: ft=zsh
