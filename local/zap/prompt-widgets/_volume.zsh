#----------+
#          .
#  VOLUME  .
#          .
#----------+
local volume_state_pcm
local volume_state_headphone
local volume_percent_pcm
local volume_percent_headphone
local volume_color_pcm
local volume_color_headphone

#-----+
# PCM .
#-----+
volume_state_pcm=$(amixer \
                    |grep -A6 PCM\
                    |grep '%')
if [[ -n $(echo $volume_state_pcm | grep off) ]]; then
    volume_percent_pcm=0
else
    volume_percent_pcm=$(echo $volume_state_pcm \
                    |grep -o '\[[0-9]\{1,3\}%\]'\
                    |grep -o '[0-9]*'\
                    |head -n1)
fi
if (( ${volume_percent_pcm} == 0 )); then
    volume_color_pcm=cyan
    volume_percent_pcm="off"
elif (( ${volume_percent_pcm} < 25 )); then
    volume_color_pcm=cyan
elif (( ${volume_percent_pcm} < 45 )); then
    volume_color_pcm=green
elif (( ${volume_percent_pcm} < 65 )); then
    volume_color_pcm=yellow
elif (( ${volume_percent_pcm} < 85 )); then
    volume_color_pcm=magenta
else
    volume_color_pcm=red
fi

#-----------+
# Headphone .
#-----------+
volume_state_headphone=$(amixer \
                        |grep -A6 Headphone\
                        |grep '%')
if [[ -n $(echo $volume_state_headphone | grep off) ]]; then
    volume_percent_headphone=0
else
    volume_percent_headphone=$(echo $volume_state_headphone \
                    |grep -o '\[[0-9]\{1,3\}%\]'\
                    |grep -o '[0-9]*'\
                    |head -n1)
fi
if (( ${volume_percent_headphone} == 0 )); then
    volume_color_headphone=cyan
    volume_percent_headphone="off"
elif (( ${volume_percent_headphone} < 25 )); then
    volume_color_headphone=cyan
elif (( ${volume_percent_headphone} < 45 )); then
    volume_color_headphone=green
elif (( ${volume_percent_headphone} < 65 )); then
    volume_color_headphone=yellow
elif (( ${volume_percent_headphone} < 85 )); then
    volume_color_headphone=magenta
else
    volume_color_headphone=red
fi
volume_prompt="%{$fg_bold[blue]%}|%{$fg_bold[white]%}vol \
%{$fg_bold[$volume_color_pcm]%}$volume_percent_pcm \
%{$fg_bold[$volume_color_headphone]%}$volume_percent_headphone\
%{$fg_bold[blue]%}|%{$reset_color%} "
volume_prompt_size=$((${#volume_percent_pcm} + ${#volume_percent_headphone} + ${#:-|vol | }))
if [[ -n $PROMPT_DEBUG ]]; then
    echo "volume size: $volume_prompt_size"
fi
