local mail_color
local mail_color_inbox
local mail_string=0
local mail_prompt_size=7
local mail_number=$(ls /home/kb/mail/**/new \
                    |grep '^[0-9]'\
                    |wc -l)
local mail_number_inbox=$(ls /home/kb/mail/IN--inbox/new \
                    |grep '^[0-9]'\
                    |wc -l)
(( mail_prompt_size += ${#mail_number}))
if [[ $mail_number > 0 ]]; then
    mail_string=$mail_number
    mail_color="%{$fg_bold[green]%}"
else
    mail_color="%{$fg_bold[red]%}"
fi
# mail_number=$(printf "%2d" $mail_number)
mail_prompt="%{$fg_bold[blue]%}|${fg_bold[white]}msg $mail_color$mail_string%{$fg_bold[blue]%}| "

if [[ -n $PROMPT_DEBUG ]]; then
    echo "mail size: $mail_prompt_size"
fi
# vim: ft=zsh
