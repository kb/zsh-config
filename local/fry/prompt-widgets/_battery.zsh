#-----------+
#           .
#  BATTERY  .
#           .
#         _ ._  _ , _ ._
#       (_ ' ( `  )_  .__)
#     ( (  (    )   `)  ) _)
#    (__ (_   (_ . _) _) ,__)
#       `~~`\ ' . /`~~`
#        ,::: ;   ; :::,
#       ':::::::::::::::'
#  _[ BUG ]__/_ __ \__________
# |                           |
# | Somehow, this script      |
# | makes zsh segfault !      |
# |___________________________|
# TODO
# Implement a graph it with these:
# X="▁▂▃▄▅▆▇█"
# X="▁▂▃▄    "
battery_prompt=
battery_prompt_size=14
local battery_capacity=$(cat /proc/acpi/battery/BAT0/info\
            |grep "last full capacity:"\
            |grep -o '[0-9]\+') 
local battery_remaining=$(cat /proc/acpi/battery/BAT0/state \
            |grep "remaining capacity:" \
            |grep -o '[0-9]\+') 
local battery_state=$(cat /proc/acpi/battery/BAT0/state\
            |G "charging state"\
            |cut -d: -f2\
            |G -o '[^ \t]\+')
case $battery_state in 
    "discharging")
        battery_state_sigil="%{$fg_bold[red]%}"
        battery_state_sigil+="↓"
    ;;
    "charging")
        battery_state_sigil="%{$fg_bold[yellow]%}"
        battery_state_sigil+="↑"
    ;;
    "charged")
        battery_state_sigil="%{$fg_bold[green]%}"
        battery_state_sigil+="*"
    ;;
esac
battery_percent=$(($battery_remaining*100.0/$battery_capacity))
battery_percent=$(printf "%5.2f" $battery_percent )
if (( $battery_percent == 100 )); then
    battery_percent=$(printf "%d" $battery_percent )
    ((battery_prompt_size -=2 ))
fi

if (( $battery_percent > 60 )); then
    battery_color="green"
elif (( $battery_percent > 40 )); then
    battery_color="yellow"
elif (($battery_percent < 15 )); then
    battery_color="red"
    if [[ $battery_state == "discharging" ]]; then
        battery_percent="▒$battery_percent ▒"
        ((battery_prompt_size += 3))
    fi
else
    battery_color="%{$fg_bold[yellow]%}"
fi
battery_prompt="%{$fg_bold[blue]%}|%{$fg_bold[white]%}bat %{$fg_bold[$battery_color]%}"
battery_prompt+="$battery_percent $battery_state_sigil%{$fg_bold[blue]%}| "
# vim: ft=zsh
