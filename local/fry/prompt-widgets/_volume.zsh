#----------+
#          .
#  VOLUME  .
#          .
#----------+
volume_state=$(amixer \
                |grep -A6 LineOut\
                |grep '[0-9]\{1,3\}%')
if [[ -z $volume_state ]]; then
    volume_prompt="%{$fg_bold[blue]%}|%{$fg_bold[white]%}vol %{$fg_bold[red]???%}%{$fg_bold[blue]%}| "
    volume_prompt_size=10
    return 0
fi
       

if [[ -n $(echo $volume_state | grep off) ]]; then
    volume_percent=0
else
    volume_percent=$(echo $volume_state \
                    |grep -o '\[[0-9]\{1,3\}%\]'\
                    |grep -o '[0-9]*'\
                    |head -n1)
fi
if (( ${volume_percent} == 0 )); then
    volume_color=%{$fg_bold[cyan]%}
    volume_percent="off"
elif (( ${volume_percent} < 25 )); then
    volume_color=%{$fg_bold[cyan]%}
elif (( ${volume_percent} < 45 )); then
    volume_color=%{$fg_bold[green]%}
elif (( ${volume_percent} < 65 )); then
    volume_color=%{$fg_bold[yellow]%}
elif (( ${volume_percent} < 85 )); then
    volume_color=%{$fg_bold[magenta]%}
else
    volume_color=%{$fg_bold[red]%}
fi
volume_prompt="%{$fg_bold[blue]%}|${fg_bold[white]}vol $volume_color$volume_percent%{$fg_bold[blue]%}| "
volume_prompt_size=$((${#volume_percent} + 6))

