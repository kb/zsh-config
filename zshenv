# export ZSH_CONFIG_DIR=$HOME/.zsh
export ZSH_CONFIG_DIR_LOCAL=$HOME/.zsh/local/$HOST

# Less settings
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;40;30m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

# NOTE: HACK
# try finding the colon-separated MANPATH
MAYBE_MANPATH=$(manpath 2>/dev/null) # for man version 2.51.
if (( $? )); then
    # it didn't work, try with man -w (v.1.6)
    MAYBE_MANPATH=$(man -w) 
    if (( $? )); then
        MANPATH
    else
        typeset -U manpath
        MANPATH=$MAYBE_MANPATH
        manpath=( $ZSH_CONFIG_DIR/doc ~/local/share/man $manpath)
    fi
else
    typeset -U manpath
    MANPATH=$MAYBE_MANPATH
    manpath=( ~/local/share/man $manpath)
fi
# lists manpath as man(1) actually uses it 
# I consider this to be clever, might therefore be stupid)
# if [[ -n $MANPATH ]]; then
    # MANPATH=$MAYBE_MANPATH
# else
    # unset manpath
    # unset MANPATH
# fi
# echo "sourced zshenv"
