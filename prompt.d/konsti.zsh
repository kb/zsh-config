function title() {
  # escape '%' chars in $1, make nonprintables visible
  a=${(V)1//\%/\%\%}

  # Truncate command, and join lines.
  a=$(print -Pn "%40>...>$a" | tr -d "\n")

  case $TERM in
  screen*)
    # truncate even further
    a=$(print -Pn "%15>...>$a" | tr -d "\n")
    # arg=$(print -Pn "%-10>...>${a:5}" | tr -d "\n")
    # a=$cmd$arg

    # print -Pn "\e]2;${a} @ $2\a" # plain xterm title
    print -Pn "\ek$a\e\\"      # screen title (in ^A")
    # print -Pn "\e_$2   \e\\"   # screen location
    ;;
  xterm*|rxvt*)
    print -Pn "\e]2;$a @ $2\a" # plain xterm title
    ;;
  esac
}

function preexec() {
  title "$1" "%m(%35<...<%~)"
}

function precmd {

local TERMWIDTH
(( TERMWIDTH = ${COLUMNS} - 1 ))

#---------------------------------------+
#                                       .
#  Load host-specific prompt variables  .
#  DISABLED                             .
#                                       .
#---------------------------------------+
widget_size=0
widgets_loaded=()
widgets_to_load=( volume battery mail )
if [[ -n PROMPT_ENABLE_VCS ]]; then
    widgets_to_load=( $widgets_to_load vcs )
fi
for widget in $widgets_to_load; do
    local widget_rc="$ZSH_CONFIG_DIR_LOCAL/prompt-widgets/$widget.zsh"
    if [[ -e $widget_rc ]]; then
        source $widget_rc
        widgets_loaded=($widget $widgets_loaded)
        widget_prompt_size=${widget}_prompt_size
        ((widget_size=$widget_size + ${widget_prompt_size}))
    else 
        set ${widget}_prompt=""
    fi
done

PR_PWDLEN=""
PR_FILL_PATTERN=''

local promptsize=${#${(%):-()%n@%m}}
local pwdsize=${#${(%):-%~}}
((promptsize = $TERMWIDTH - 2 - ($promptsize + $pwdsize + $widget_size )))
PR_FILLBAR2=" \${(l.$promptsize..$PR_FILL_PATTERN.)} "
if [[ "$promptsize + $pwdsize" -gt $TERMWIDTH ]]; then
    ((PR_PWDLEN=$TERMWIDTH - $promptsize))
fi

# title="${PWD} (${USER}@${HOST})"
# title "zsh" "%m(%55<...<%~)"
}

setprompt () {
    setopt prompt_subst
    # no colors if inside of midnight cmmander
    if [[ -n "$MC_SID" ]]; then
        local fg=
        local fg_bold=
        local reset_color=
    fi
    
PROMPT='%{$fg_bold[blue]%}(%{$fg_bold[white]%}%$PR_PWDLEN<...<%~%<<%{$fg_bold[blue]%})%{$reset_color%}'
PROMPT+='$vcs_prompt'
PROMPT+='${(e)PR_FILLBAR2}'
PROMPT+='$mail_prompt'
PROMPT+='$volume_prompt'
PROMPT+='$battery_prompt'
PROMPT+='%{$fg_bold[magenta]%}%n%{$fg_no_bold[magenta]%}@%{$fg_bold[magenta]%}%m%{$reset_color%}
%{$fg_bold[blue]%}(%{$fg_bold[yellow]%}%D{%H:%M}\
%{$fg_bold[blue]%})\
%{$fg_bold[white]%}> %{$reset_color%}'
    # ☠✔✓✱✳✸✿❍❡❦❤➜↓➤★☉☑☒
    OKSIGN="无"
    # OKSIGN="…"
    NOTOKSIGN_L="▶ "
    NOTOKSIGN_R=" ◀"
    RPROMPT='%(?.%{$fg[green]%}$OKSIGN.%{$fg_bold[blue]%}$NOTOKSIGN_L%{$fg_bold[red]%}%?%{$fg_bold[blue]%}$NOTOKSIGN_R)%{$reset_color%}'

PS2='%{$fg_bold[blue]%}(%{$fg_bold[green]%}%_%{$fg_bold[blue]%}) %{$reset_color%}'

}

if (($COLUMNS > 20));then
    setprompt
else
    PROMPT="HAHU"
fi
# vim: ft=zsh
